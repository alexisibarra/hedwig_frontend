# Requerimientos

Este proyecto asume que las siguientes herramientas ya están instaladas:

  * [NodeJS 0.12+](https://nodejs.org)
  * [AngularJS](https://angularjs.org/)
  * [Bower](http://bower.io)
  * [Gulp](http://gulpjs.com)

# Instalación para desarrolladores
Para instalar este proyecto, primero, debe clonar el código desde el repositorio:

	$ git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/hedwig-frontend

Dependiendo del ambiente en el cual vaya a hacer el despliegue, copie el archivo `env.example.json` a `env.<enviroment>.json` y configurelo adecuadamente

Luego, instalar todas las dependencias:

	$ npm install

	$ bower install

Para ejecutar un servidor de desarrollo:

	$ grunt dev

# Despliegue

Este proyecto es desplegado usando **Amazon S3** 

**aws-cli** es necesario para realizar el despliegue adecuadamente a **Amazon S3**, para mas información [ingrese acá](https://aws.amazon.com/es/cli/).
**aws-cli** debe ser configurara adecuadamente con las credenciales para llevar a cabo este paso

**Importante**: El bucket en el cual este proyecto vaya a ser desplegado debe estar configurado con los permisos adecuados, de otra manera obtendrá un error **403 Forbidden**. La siguiente policy resuelve esto:

 ```
    {
    	    "Version": "2012-10-17",
   	      "Statement": [{
   		        "Sid": "AddPerm",
   		        "Effect": "Allow",
     		      "Principal": "",
   	  	      "Action": [
   		  	        "s3:GetObject"
   		        ],
   		        "Resource": [
   			          "arn:aws:s3:::bucket-name/"
     		      ]
   	      }]
      }
 ```

Luego puede ejecutar el script **deploy.sh** para desplegar la aplicación
