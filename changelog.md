# 1.2.0
- [feature] Adición de imagenes con tags para un artículo
- [bugfix] las categorizaciones no se guardaban al momento de editar/crear un artículo
- [support] Desactivada la carga masiva
- [bugfix] No se guardan los tags ni titulos alternativos con repeticiones para un artículo
- [bugfix] Corrección de los botones en los formularios. Todos tienen la etiqueta 'Enviar'
- [bugfix] Corregido el campo de tweets existentes al momento de editar artículo. No permite agregar, solo eliminar.
- [bugfix] Corrección de botones 'Ver Activos' y 'Ver inactivos' en viste de artículos: estaban invertidos.

# 1.2.2
- [bugfix] Las categorizaciones deben ser creadas con nombre unico
- [bugfix] Los usuarios deben ser creados con username unico
- [bugfix] Los usuarios deben ser creados con email unico
- [bugfix] Titulo y descripcion genericos para la aplicacion
- [feature] Marcar articulo como destacado

# 1.3.0
- [feature] despliega los tags de las imagenes en la vista de artículo
- [feature] recuperación de contraseña

# 1.3.1
- [bugfix] Eliminando la injeccion por error de un modulo en el controlador de usuario

# 1.3.2
- [bugfix] Botón guardar de la edición de categorías: ahora dice 'enviar'
- [bugfix] Se pueden subir imágenes de más de 1MB para un artículo
- [bugfix] Se puede subir cualquier archivo de cualquier tipo para un artículo

# 1.4.1
- [feature] Se valida que el usuario no introduzca social Media repetidos
- [feature] Se agregan botones para eliminar nuevos social media
- [feature] Se modifica el consumo del endpoint massiveLoad para incluir el id del usuario al cual se le adjudicara la autoria de los articulos
- [bugfix] Se puede eliminar, activar y editar desde la vista de artículo
- [feature] Cambio de contraseña

# 1.5.0
- [feature] Paginado de usuarios
- [feature] Paginado de categorizaciones
- [feature] Paginado de artículos
- [feature] Busqueda de usuarios
- [feature] Busqueda de items

# 1.5.2
- [feature] Añade campo de configuración para propiedad colectiva
- [feature] Añade campo de creditos para li-news
- [feature] muestra periodo de actividad de un artículo
- [feature] sección articulos por categorias
- [feature] Manejo de imágenes obligatorias para un artículo

# 1.6.0
- [feature] cambia la propiedad destacado de las li-news a una zona y elimina los metodos relacionados a este

# 1.6.1
- [feature] inicializa froala con el hashkey de s3 para subir imagenes

# 1.6.4
- [bugfix] Se procesa el formulario de creacion de articulos solo si este es valido. De lo contrario se muestra mensajs de error
- [feature] Se muestra toast si el formulario de creacion de nota hace submit y no es valido
- [feature] Se implementan servicios para refactorizar controladores
- [feature] Inhibe el boton de guardar una vez que se ha hecho click la primera vez en los formularios

## Futuras versiones
- [feature] la configuración de la aplicación se realiza empleando información desde el API
