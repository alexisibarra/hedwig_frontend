# Features del frontend

## Manejo de sesión y perfil de usuario
- Inicio de sesión
- Cierre de sesión
- Recuperación de contraseña
- Cambio de contraseña

## Manejo de usuarios
- Creación de usuarios
- Ver todos los usuarios
    + Se debe poder diferenciar entre usuarios activos y no activos
    + Se debe permitir ir al detalle de cada usuario
- Ver usuario en detalle
- Actualizar usuario
- Eliminar usuario
- Modificación obligatoria de contraseña

## Manejo de artículos
- Creación de artículo
- Pre-visualización del artículo
- Ver todos los artículos
- Ver artículo en detalle
- Actualizar artículo
- Eliminar artículo

### Manejo de multimedia
#### Manejo de imagenes
+ Carga de imagen 
    + Pre-visualización de la imagen
+ Ver todas las imagenes disponibles
    * Ver en formato cuadricula
    * Ver en formato lista
+ Ver imagen en detalle
+ Eliminar imagen

#### Manejo de multimedia social
- Cargar un tweet
- Ver un tweet
- Actualizar un tweet
- Eliminar un tweet
- Cargar un video de youtube    
- Ver un video de youtube    
- Actualizar un video de youtube    
- Eliminar elemento de multimedia social
  
## Manejo de categorias
- Creación de categoría
- Listar categorias
- Ver categoría
- Editar categoría
- Eliminar categoría
