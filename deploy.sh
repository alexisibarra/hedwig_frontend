if [ $1 ]
	then

		if [ $1 = "LI" ]; then
			bucket="s3://ed2.laiguana.tv"
		else
			bucket="s3://sologenuino-editor"
		fi
		git co master
		grunt setprod
		aws s3 cp . $bucket \
			--region us-east-1 \
			--exclude "node_modules/*" \
			--exclude "doc/*" \
			--exclude ".git/*" \
		 	--exclude "deploy.sh" \
		 	--exclude ".idea/*" \
		 	--exclude ".DS_Store" \
		 	--exclude "*/.DS_Store" \
		 	--exclude "app/assets/" \
		 	--exclude "bootstrap.less" \
		 	--exclude "env.json" \
		 	--exclude "env.*.json" \
		 	--exclude "aws-keys.json" \
		 	--exclude ".bowerrc" \
		 	--exclude ".bumpversion.cfg" \
		 	--exclude ".editorconfig" \
		 	--exclude ".gitignore" \
		 	--exclude ".jshintrc" \
		 	--exclude ".yo-rc.json" \
		 	--exclude "bower.json" \
		 	--exclude "changelog.md" \
		 	--exclude "deploy.sh" \
		 	--exclude "Gruntfile.js" \
		 	--exclude "karma.conf.js" \
		 	--exclude "LICENCE.md" \
		 	--exclude "package.json" \
		 	--exclude "README.md" \
		 	--exclude "VERSION" \
		 	--exclude "setEnvironmentForClient.sh" \
		 	--recursive --acl public-read
		 git co -
	else
		echo "Debe indicar el bucket al cual quiere hacer el deploy"
fi
