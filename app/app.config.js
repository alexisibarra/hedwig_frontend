(function () {
	'use strict';

	/**
	 * @ngdoc configuration file
	 * @name app.config:config
	 * @description
	 * # Config and run block
	 * Configutation of the app
	 */


	angular
		.module('hedwig')
		.config(configureBlock)
		.run(runBlock);

	configureBlock.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'RestangularProvider', 'ENV'];

	function configureBlock($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, RestangularProvider, ENV) {
		RestangularProvider.setBaseUrl(ENV.API_URL);

		$locationProvider.hashPrefix('!');

		// This is required for Browser Sync to work poperly
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

		$urlRouterProvider
			.otherwise('/dashboard');

	}

	runBlock.$inject = ['$rootScope', 'PermPermissionStore', 'PermRoleStore', 'AuthService', 'authEvents', 'Restangular', 'ENV'];

	function runBlock($rootScope, PermPermissionStore, PermRoleStore, AuthService, authEvents, Restangular, ENV) {
		'use strict';

		var item_type = ENV.item_type;

		PermPermissionStore.definePermission('isAdmin', function () { return AuthService.isAdmin();});

		PermPermissionStore.definePermission(
			'canViewArticle',
			function (permissionName, transitionProperties) {
				var filterObject = {'filter[include]': 'creatorUser'};

				return Restangular
					.one(item_type, transitionProperties.toParams.id)
					.get(filterObject)
					.then(function(article){
						if(article.creatorUserId !== parseInt(AuthService.getUserId())){
							throw authEvents.NOT_OWNER;
						};
					})
					.catch(function(err){
						throw(err);
					});

			}

		);
		PermPermissionStore.definePermission(
			'collectiveProperty',

			function (permissionName, transitionProperties) {
				return ENV.collectiveProperty;
			}

		);

		PermRoleStore
			.defineManyRoles({
				'LOGGEDIN': function () { return AuthService.isAuthenticated(); },
				'ADMIN': ['createArticle', 'viewArticle'],
				'EDITOR': ['createArticle'],
			});

		console.log('AngularJS run() function...');
	}


})();
