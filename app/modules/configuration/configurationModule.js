(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:configurationModule
	 * @description
	 * # configurationModule
	 * Module of the app
	 */

	angular.module('configuration', []);

})();
