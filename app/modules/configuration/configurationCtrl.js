(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:categoriesCtrl
	* @description
	* # categoriesCtrl
	* Controller of the app
	*/

	angular
		.module('configuration')
		.controller('ConfigurationCtrl', Configuration);

		Configuration.$inject = ['$scope', 'Restangular'];

		function Configuration($scope, Restangular) {
			/*jshint validthis: true */
			var vm = this;
            $scope.config = {};
            var config;

            var syncConfig = function(){
                config = Restangular
                    .all("configs")
                    .getList();

                config.then(function(response){
                    $scope.config.home_active = response[0].home_active;
                    $scope.config.article_active = response[0].article_active;
                });
            }

            syncConfig();

            $scope.changeHomeActive = function(){
                changeActive("home");
            }

            $scope.changeArticleActive = function(){
                changeActive("article");
            }

            var changeActive = function(section){
                return config
                    .then(function(config) {
                        var config = config[0]; 
                        config[section + "_active"] = $scope.config[section + "_active"];
                        config.put();
                    })
            }
		}
})();
