'use strict';

/**
 * @ngdoc function
 * @name app.route:configurationRoute
 * @description
 * # configurationRoute
 * Route of the app
 */

angular.module('configuration')
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			.state('home.config', {
				url:'/config',
				templateUrl: 'app/modules/configuration/config.html',
				controller: 'ConfigurationCtrl',
				controllerAs: 'vm',
				data: {
					permissions: {
						only: ['isAdmin'],
						redirectTo: "home.login"
					}
				}
			});
	}]);
