(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:usersCtrl
	* @description
	* # usersCtrl
	* Controller of the app
	*/

	angular
		.module('users')
		.controller('UsersCtrl', Users);

		Users.$inject = ['$scope', '$state', '$stateParams', '$mdDialog', '$mdToast', 'Restangular', 'lodash', 'ToastService'];

		/*
		* recommend
		* Using function declarations
		* and bindable members up top.
		*/

		function Users($scope, $state, $stateParams, $mdDialog, $mdToast, Restangular, _, ToastService) {
			/*jshint validthis: true */
			var vm = this;

			$scope.limit = 10;
			$scope.currentPage = 0;

			$scope.pageChanged = function() {
				vm.loadUsers();
			};

			vm.loadUsers = function(){
				var skip = $scope.currentPage > 0 ? ($scope.currentPage-1)*$scope.limit : $scope.currentPage*$scope.limit;

				var filterObject = {'filter[limit]': 10, 'filter[skip]': skip};

				var baseUsers = Restangular.all("editorusers");

				baseUsers = vm.search ?
					baseUsers.customGET("search/" + $stateParams.searchQuery, filterObject):
					baseUsers.getList(filterObject) ;

				baseUsers
					.then(function(users){
						$scope.users = users;
					});
			};

			$scope.edit = false;
			vm.search = false;

			if($state.current.name === 'home.users'){
				vm.search = false;

				Restangular
					.all("editorUsers")
					.customGET("count")
					.then(function(response){
						$scope.totalItems = response.count;
					});

				vm.loadUsers();
			} else if($state.current.name === 'home.newuser'){
				vm.search = false;

				vm.user = {};
			} else if($state.current.name === 'home.edituser'){
				$scope.edit = true;
				vm.search = false;

				Restangular
					.one("editorusers", $stateParams.id)
					.get()
					.then(function(user){
						vm.user = user;
					});
			} else if($state.current.name === 'home.usersSearch'){
				vm.search = true;
				vm.searchQuery = $stateParams.searchQuery;

				Restangular
					.all("editorusers")
					.customGET("searchCount/" + vm.searchQuery)
					.then(function(response){
						$scope.totalItems = response.count;
					});

				vm.loadUsers();
			}

			vm.ProcessForm = function(){
				var userOperation = $scope.edit ? vm.user.put() : Restangular.all("editorusers").post(vm.user);

				userOperation
					.then(function(response){
						$state.go("home.users");

						var action = $scope.edit ? "editado" : "creado";

						console.log("Usuario " + action);
						ToastService.showSimpleToast("Usuario " + action);
					})
					.catch(function(err){
						switch(err.status){
							case 422:
								_.each(err.data.error.details.messages, function(value){
									ToastService.showSimpleToast(value[1]);
								});
								break;
							default:
								console.log(err);
						}
					});
			};

			vm.deleteConfirm = function(user) {
				// Appending dialog to document.body to cover sidenav in docs app
				var confirm = $mdDialog.confirm()
					.title('¿Quiere eliminar el usuario \'' +user.fullname +'\'?')
					.textContent('Esta acción es irreversible')
					.ariaLabel('Lucky day')
					.ok('Si, eliminalo')
					.cancel('No, cancela');

				$mdDialog.show(confirm).then(function() {
					$scope.status = 'You decided to get rid of your debt.';

					Restangular
						.one("editorusers", user.id)
						.remove()
						.then(function(user){
							console.log("Usuario eliminado", user);
							vm.loadUsers();
							ToastService.showSimpleToast("Usuario eliminado");
						})
						.catch(function(err){
							console.log(err);
						});

				}, function() {
					$scope.status = 'You decided to keep your debt.';
				});
			};
		}
})();
