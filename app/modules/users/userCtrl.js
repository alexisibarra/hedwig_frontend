(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name app.controller:usersCtrl
     * @description
     * # usersCtrl
     * Controller of the app
     */

    angular
        .module('users')
        .controller('UserCtrl', Users);

    Users.$inject = ['$stateParams', '$scope', 'Restangular', 'lodash'];

    function Users($stateParams, $scope, Restangular, _) {
        /*jshint validthis: true */
        var vm = this;

        const reloadUser = function(){
            $scope.hasRoles = [];
            
            Restangular
                .one("editorusers", $stateParams.id)
                .get({'filter[include]': 'roles'})
                .then(function(user){
                    _.each(user.roles, function(role){
                        $scope.hasRoles[role.name] = true;
                    });

                    $scope.user = user;
                });
        };

        reloadUser();

		Restangular
			.all("Roles")
			.getList()
            .then(function(response){
                $scope.roles = response;
            });

        $scope.manageRole = function (hasit, roleId){
            if(hasit){
                Restangular
                    .one("RoleMappings")
                    .get({'filter[where][roleId]': roleId, 'filter[where][principalId]': $scope.user.id})
                    .then(function(response){
                        return Restangular
                            .one("RoleMappings", response[0].id)
                            .remove();
                    })
                    .then(function(response){
                        reloadUser();
                    });
            } else {
                Restangular
                    .all("RoleMappings")
                    .post({roleId: roleId, principalId: $scope.user.id})
                    .then(function(response){
                        reloadUser();
                    });
                console.log("Role Added");            
            }
        };
    }

})();
