(function() {
	'use strict';

	angular
		.module('articles')
		.factory('FroalaService', froalaService);

		froalaService.$inject = [];

		function froalaService () {
            var froalaService = {};

            froalaService.setMultimediaPopUp = function(){
                $.extend($.FroalaEditor.POPUP_TEMPLATES, {
                    "customPlugin.popup": '[_BUTTONS_][_CUSTOM_LAYER_]'
                });
    
                // Define popup buttons.
                $.extend($.FroalaEditor.DEFAULTS, {
                    popupButtons: ['popupClose', '|', 'insert'],
                });
            
                // The custom popup is defined inside a plugin (new or existing).
                $.FroalaEditor.PLUGINS.customPlugin = function (editor) {
                    // Create custom popup.
                    function initPopup () {
                        // Popup buttons.
                        var popup_buttons = '';

                        // Create the list of buttons.
                        if (editor.opts.popupButtons.length > 1) {
                            popup_buttons += '<div class="fr-buttons">';
                            popup_buttons += editor.button.buildList(editor.opts.popupButtons);
                            popup_buttons += '</div>';
                        }

                        // Load popup template.
                        var template = {
                            buttons: popup_buttons,
                            custom_layer: '<p>Ingrese su código:</p><input size="100" id="media-insert-popup" onkeypress="return event.keyCode != 13;">'
                        };

                        // Create popup.
                        var $popup = editor.popups.create('customPlugin.popup', template);

                        return $popup;
                    }

                    // Show the popup
                    function showPopup () {
                        // Get the popup object defined above.
                        var $popup = editor.popups.get('customPlugin.popup');
                        // this.html.insert('<iframe width="560" height="315" src="https://www.youtube.com/embed/hPC2Fp7IT7o" frameborder="0" allowfullscreen></iframe>');

                        // If popup doesn't exist then create it.
                        // To improve performance it is best to create the popup when it is first needed
                        // and not when the editor is initialized.
                        if (!$popup) $popup = initPopup();

                        // Set the editor toolbar as the popup's container.
                        editor.popups.setContainer('customPlugin.popup', editor.$tb);

                        // This will trigger the refresh event assigned to the popup.
                        // editor.popups.refresh('customPlugin.popup');

                        // This custom popup is opened by pressing a button from the editor's toolbar.
                        // Get the button's object in order to place the popup relative to it.
                        var $btn = editor.$tb.find('.fr-command[data-cmd="addMultimediaCode"]');

                        // Set the popup's position.
                        var left = $btn.offset().left + $btn.outerWidth() / 2;
                        var top = $btn.offset().top + (editor.opts.toolbarBottom ? 10 : $btn.outerHeight() - 10);

                        // Show the custom popup.
                        // The button's outerHeight is required in case the popup needs to be displayed above it.
                        editor.popups.show('customPlugin.popup', left, top, $btn.outerHeight());
                    }

                    // Hide the custom popup.
                    function hidePopup () {
                        editor.popups.hide('customPlugin.popup');
                    }

                    // Methods visible outside the plugin.
                    return {
                        showPopup: showPopup,
                        hidePopup: hidePopup
                    }
                }	
            
                // Define an icon and command for the button that opens the custom popup.
                $.FroalaEditor.DefineIcon('buttonIcon', { NAME: 'code'})
                $.FroalaEditor.RegisterCommand('addMultimediaCode', {
                    title: 'Agregar multimedia externo',
                    icon: 'buttonIcon',
                    undo: false,
                    focus: false,
                    plugin: 'customPlugin',
                    callback: function () {
                    this.customPlugin.showPopup();
                    }
                });
            
                // Define custom popup close button icon and command.
                $.FroalaEditor.DefineIcon('popupClose', { NAME: 'times' });
                $.FroalaEditor.RegisterCommand('popupClose', {
                    title: 'Cerrar',
                    undo: false,
                    focus: false,
                    callback: function () {
                    this.customPlugin.hidePopup();
                    }
                });
            
                // Define custom popup 1.
                $.FroalaEditor.DefineIcon('insert', { NAME: 'plus' });
                $.FroalaEditor.RegisterCommand('insert', {
                    title: 'Insertar',
                    undo: false,
                    focus: false,
                    callback: function () {
                        this.html.insert($('#media-insert-popup').val());
                    }
                });
            }

            froalaService.getFroalaCreditsOptions = function(){
                return {
				height: 100,
				toolbarButtons: [
					'insertLink'
				],
				linkList: [
					{
						text: '(LaIguana.tv)',
						href: 'https://laiguana.tv',
						target: '_blank'
					},
					{
						text: '(AFP)',
						href: '',
						target: '_blank'
					},
					{
						text: '(AVN)',
						href: '',
						target: '_blank'
					},
					{
						text: '(Agencias)',
						href: '',
						target: '_blank'
					},
					{
						text: '(EFE)',
						href: '',
						target: '_blank'
					},
					{
						text: '(AP)',
						href: '',
						target: '_blank'
					},
					{
						text: '(REUTERS)',
						href: '',
						target: '_blank'
					},
					{
						text: '(aporrea.org)',
						href: 'aporrea.org',
						target: '_blank'
					},
					{
						text: '(ciudadccs.info)',
						href: 'ciudadccs.info',
						target: '_blank'
					},
					{
						text: '(correodelorinoco.gob.ve)',
						href: 'correodelorinoco.gob.ve',
						target: '_blank'
					},
					{
						text: '(contrainjerencia.com)',
						href: 'contrainjerencia.com',
						target: '_blank'
					},
					{
						text: '(conelmazodando.com.ve)',
						href: 'conelmazodando.com.ve',
						target: '_blank'
					},
					{
						text: '(cubadebate.cu)',
						href: 'cubadebate.cu',
						target: '_blank'
					},
					{
						text: '(globovision.com)',
						href: 'globovision.com',
						target: '_blank'
					},
					{
						text: '(telesurtv.net)',
						href: 'telesurtv.net',
						target: '_blank'
					},
					{
						text: '(noticias24.com)',
						href: 'noticias24.com',
						target: '_blank'
					},
					{
						text: '(panorama.com.ve)',
						href: 'panorama.com.ve',
						target: '_blank'
					},
					{
						text: '(radiomundial.com.ve)',
						href: 'radiomundial.com.ve',
						target: '_blank'
					},
					{
						text: '(actualidad.rt.com)',
						href: 'actualidad.rt.com',
						target: '_blank'
					},
					{
						text: '(ultimasnoticias.com.ve)',
						href: 'ultimasnoticias.com.ve',
						target: '_blank'
					},
					{
						text: '(lechuguinos.com)',
						href: 'lechuguinos.com',
						target: '_blank'
					},
					{
						text: '(unionradio.net)',
						href: 'unionradio.net',
						target: '_blank'
					},
					{
						text: '(vtv.gob.ve)',
						href: 'vtv.gob.ve',
						target: '_blank'
					},
					{
						text: '(latabla.com)',
						href: 'latabla.com',
						target: '_blank'
					},
					{
						text: '(noticiascarabobo24.com)',
						href: 'noticiascarabobo24.com',
						target: '_blank'
					},
					{
						text: '(notitarde.com)',
						href: 'notitarde.com',
						target: '_blank'
					},
					{
						text: '(Nota de Prensa)',
						href: 'Nota de Prensa',
						target: '_blank'
					},
					{
						text: '(pijamasurf.com)',
						href: 'pijamasurf.com',
						target: '_blank'
					},
					{
						text: '(Clodovaldo Hernández)',
						href: '',
						target: '_blank'
					}
				]
			}
            }
            return froalaService;
		};
})();
