(function() {
	'use strict';

	angular
		.module('articles')
		.factory('CategorizationsService', categorizationsService);

		categorizationsService.$inject = ['$q', 'ENV', 'Restangular'];

		function categorizationsService (q, ENV, Restangular) {
			var categorizationsService = {};

			categorizationsService.load = function() {
				var categorizations = Restangular.all("categorizations");

				return q.all([
					categorizations.customGET("categories"),
					categorizations.customGET("zones"),
					categorizations.customGET("censuras")
				])
			};

			return categorizationsService;
		};
})();
