(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:articlesCtrl
	* @description
	* # articlesCtrl
	* Controller of the app
	*/

	angular
		.module('articles')
		.controller('ArticlesCtrl', Articles);

		Articles.$inject = [
            'CategorizationsService', '$scope', '$state', '$stateParams', 'lodash', '$q', '$http', 'ArticlesService',
            'ENV', 'Restangular', 'ToastService', 'UtilsService', '$sce', 'AuthService', 'FroalaService'
        ];

		function Articles(
            CategorizationsService, $scope, $state, $stateParams, _, q, $http, ArticlesService,
            ENV, Restangular, ToastService, UtilsService, $sce, AuthService, FroalaService
        ) {
			/*jshint validthis: true */
			var vm = this;
			var item_type = ENV.item_type;
			$scope.config = {};

			$scope.item_name = ENV.item_name;

			$scope.config.showAltTitles = ENV.showAltTitles;
			$scope.config.showDates = ENV.showDates;

			$scope.Zones = [];
			$scope.Categories = [];
			$scope.Censuras = [];

			$scope.previousCategories = [];
			$scope.previousZones = [];
			$scope.previousCensuras = [];
			$scope.deleteCategorizations = [];
			$scope.addCategorizations = [];

			$scope.showSocialMultimedias = ENV.social_multimedia.length > 0;

			_.each(ENV.social_multimedia, function(mediaName){
				$scope['show' + _.upperFirst(mediaName) + 's'] = true;
				$scope['valid' + _.upperFirst(mediaName) + 's'] = true;
				$scope['new' + _.upperFirst(mediaName) + 's'] = [];
				$scope['delete' + _.upperFirst(mediaName) + 's'] = [];												
			});

            $scope.image_tags = ENV.image_tags;

            $scope.$on('loadArticles', function(event, args) {
                vm.loadArticles();
            });

			$scope.cancelEdit = function(id){
				if(id){
					ArticlesService
						.unlock(id)
						.then(function(){
							$state.go("home.articles");
						})
				} else {
					$state.go("home.articles");
				}
			}

			$scope.removeEmptySocialMedia = function(mediaName, index){
				$scope['new' + _.upperFirst(mediaName) + 's'].splice(index, 1);
				$scope.checkSocialMedia(mediaName);
			};

			$scope.checkSocialMedia = function(mediaName){
				var newSocialMedia = $scope['new' + _.upperFirst(mediaName) + 's'];

				if(newSocialMedia.length == 0) return true;

				var socialMediaID = _.lowerFirst(mediaName) + 'ID';

				$scope['valid' + _.upperFirst(mediaName) + 's'] =
					_(newSocialMedia)
						.groupBy(socialMediaID)
						.reduce(function(result, value){
							return (value.length <= 1) && result;
						}, true);
			};

			$scope.checkForm = function(){
				var result = true;
				_.each(ENV.social_multimedia, function(mediaName){
					$scope.checkSocialMedia(mediaName);

					result = result && $scope['valid' + _.upperFirst(mediaName) + 's'];
				});

				let prevZonesCount = $scope.previousZones.length;
				let deleteZonesCount = _.filter($scope.deleteCategorizations, function(c){return c.type === "zona"}).length;
				let addZonesCount = _.filter($scope.addCategorizations, function(c){return c.type === "zona"}).length;

				let prevCategoriesCount = $scope.previousCategories.length;
				let deleteCategoriesCount = _.filter($scope.deleteCategorizations, function(c){return c.type === "categoria"}).length;
				let addCategoriesCount = _.filter($scope.addCategorizations, function(c){return c.type === "categoria"}).length;

				let prevCensurasCount = $scope.previousCensuras.length;
				let deleteCensurasCount = _.filter($scope.deleteCensuras, function(c){return c.type === "censura"}).length;
				let addCensurasCount = _.filter($scope.addCensuras, function(c){return c.type === "censura"}).length;

				if(vm.article.active){
					if(((prevZonesCount + addZonesCount)-deleteZonesCount) < 1) return {result: false, error: "El artículo está activo. Zonas debe tener al menos un elemento"};
					if(((prevCategoriesCount + addCategoriesCount)-deleteCategoriesCount) < 1) return {result: false, error: "El artículo está activo. Categorias debe tener al menos un elemento"};
				}

				return result ? {result: true} : {result: false, error: "SocialMedia incompleto"};
			};

			$scope.toggle = function (item, prevList) {
				var idprev = UtilsService.getItemIndex(item, prevList);
				var idadd = UtilsService.getItemIndex(item, $scope.addCategorizations);
				var iddel = UtilsService.getItemIndex(item, $scope.deleteCategorizations);

				if(idprev > -1 ) {
					if (iddel === -1){
						$scope.deleteCategorizations.push(item);
					} else {
						$scope.deleteCategorizations.splice(iddel, 1);					
					}
				} else {
					if(idadd > -1){
						if(iddel === -1 && idprev > -1)	$scope.deleteCategorizations.push(item);
							$scope.addCategorizations.splice(idadd, 1);				
					} else {
						if(iddel > -1)
							$scope.deleteCategorizations.splice(iddel, 1);					
						$scope.addCategorizations.push(item);					
					}
				}
			};

			$scope.exists = function (item, prevlist) {
				return UtilsService.itemExists(item, prevlist);
			};

			$scope.isIndeterminate = function(list) {
				return ($scope['selected' + list].length !== 0 &&
				$scope['selected' + list].length !== $scope[list].length);
			};

			$scope.isChecked = function(list) {
				return $scope['selected' + list].length === $scope[list].length;
			};

			vm.loadCategorizations = function(){
                CategorizationsService.load()
                    .then(function(response){
                        $scope.Categories = response[0];
                        $scope.Zones = response[1];
                        $scope.Censuras = response[2];
                    });
            };

			$scope.changeStatus = function(id, status){
				var action = status ? "activate" : "deactivate";

				Restangular
					.one(item_type, id)
					.customPOST({}, action, {}, {})
					.then( function(response){
						vm.loadArticles();
					});
			};

			vm.checkMandatories = function(){
				$scope.tagsNotFound = [];

				_.filter(ENV.image_tags, {mandatory: true})
					.map(function(mandatoryImage){
						var wholeImages  = _.assign({}, $scope.newImages, $scope.prevImages);
						var newWithTag = _.filter(wholeImages, {'tag': mandatoryImage.tag});

						if(_.isEmpty(newWithTag)) {
							if(_.isEmpty( _.find($scope.tagsNotFound, {tag: mandatoryImage.tag }))){
								$scope.tagsNotFound.push(mandatoryImage);
							}
						}
						else {
							_.remove($scope.tagsNotFound, function (tag) {
								return tag.tag === mandatoryImage.tag
							});
						}
					});
			};

			FroalaService.setMultimediaPopUp();

			$scope.froalaInitialization = function(initControls){
				Restangular.all("images")
					.customGET("getsignature")
					.then(function(response){
						var s3Hash = {
							"bucket": response.bucket,
							"region": response.region,
							"keyStart": response.keyStart,
							"params": response.params
						};
						let controls = [
								'paragraphFormat', '|', 'paragraphStyles', 'undo', 'redo', '|', 'bold', 'italic', 'underline', '|',
								'formatOL', 'formatUL', 'quote', 'insertHR', 'insertLink', 'insertVideo', 'insertTable',
								'selectAll', '|', 'help', '|', 'addMultimediaCode'
							];

						if(AuthService.isAdmin()){
							controls.push('|');
							controls.push('html');
						}
						var froalaOptions = {
							imageUploadToS3: s3Hash,
							theme: 'royal',
							zIndex: 2003,
							language: 'es',
							heightMin: 300,
							heightMax: 1500,
							pastePlain: true,
							toolbarButtons: controls,
							toolbarButtonsMD: controls,
							toolbarButtonsSM: controls,
							toolbarButtonsXS: controls,
							videoInsertButtons: ['videoBack', '|', 'videoByURL', 'videoEmbed'],
							linkList: [
								{
									text: '(LaIguana.tv)',
									href: 'https://laiguana.tv',
									target: '_blank'
								}
							]
						};

						$scope.froalaOptions = froalaOptions;
						initControls.initialize(froalaOptions);
					});
			};

			$scope.froalaCreditsOptions = FroalaService.getFroalaCreditsOptions();

            vm.edit = !(_.isUndefined($stateParams.id));

			$scope.newTags = [];
			$scope.newAltTitles = [];

            ArticlesService
                .getNew($stateParams.id)
                .then(function(editArticle){
                    vm.article = editArticle.article;

                    $scope.newTags = UtilsService.objectifyArray(vm.article.tags);
                    $scope.newAltTitles = UtilsService.objectifyArray(vm.article.alt_titles);
                    $scope.prevImages = vm.article.images;
                    $scope.previousCategories = _.filter(vm.article.categorization, function(c){return c.type === "categoria"});
                    $scope.previousZones = _.filter(vm.article.categorization, function(c){return c.type === "zona"});
                    $scope.previousCensuras = _.filter(vm.article.categorization, function(c){return c.type === "censura"});
                    $scope.youtubes = vm.article.youtube;
                    $scope.tweets = vm.article.tweet;

					if(vm.edit){
						if (vm.article.locked === 0) {
							console.log("No estaba bloqueada");
							
							ArticlesService
								.lock($stateParams.id)
								.then(function(response){
									console.log("Bloqueada para edición de otros usuarios");
								});
						} else if(vm.article.locked !== parseInt(AuthService.getUserId())) {
							console.log("Yo no bloqueé esta nota");
							vm.isLocked = true;							
						} 
					}

                    vm.checkMandatories();
                });

			vm.loadCategorizations();

			vm.deleteImage = function(articleId, imageId, index){
				vm.checkMandatories();
				var imageTag = $scope.prevImages[index].tag;
				// console.log(imageTag);
				console.log($scope.prevImages);
				if(vm.article.active > 0){
					if(checkValidMandatories(imageTag)){
						ToastService.showSimpleToast("No puede borrar la imagen obligatoria. El Articulo está activo. Guarde otra del mismo tipo antes de eliminar esta.");
					} else {
						deleteImage(articleId, imageId, index)
					}
				} else {
					deleteImage(articleId, imageId, index)
				}
			};

			var deleteImage = function(articleId, imageId, index){
				return	$http.delete(ENV.API_URL + '/'+ item_type +'/'+ articleId +'/deleteImage/' + imageId)
						.then(function(response){
							$scope.prevImages.splice(index, 1);

							ToastService.showSimpleToast("Imagen eliminada con éxito");
						})
						.catch(function(err){
							console.log(err);
						});
			}

			var checkValidMandatories = function(imageTag){
					var checkIfMandatory = _.filter(ENV.image_tags, {mandatory: true})
						.map(function(mandatoryImage){
							if(mandatoryImage.tag === imageTag)
								return true;
							return false;
						})
						.reduce(function(acc, n) { return acc || n});

					var checkPrevMandatories = _.reduce($scope.prevImages, function(acc, image){
						return image.tag === imageTag ? acc+1 : acc;
					}, 0) 

					var checkNewMandatories = _.reduce($scope.newImages, function(acc, image){
						return image.tag === imageTag ? acc+1 : acc;
					}, 0) 

					return checkIfMandatory && checkPrevMandatories < 2;
			}
			vm.getImageTags = function(image){
				var imageUrl = image.lfDataUrl || image.url;

				var imageDimensions = UtilsService.getImageDimensions(imageUrl);
				return _.filter(ENV.image_tags,
					function(tag) {
						if (!tag.dimensions ||
                            (
                                (imageDimensions.width === tag.dimensions.width) &&
                                (imageDimensions.height === tag.dimensions.height)
                            )) {
                            return tag;
                        }
					});
			};

			vm.changeImageTag = function(image, index){
				vm.checkMandatories();
				if(vm.article.active > 0){
					
				} 
				$http.put(ENV.API_URL + '/images/' + image.id, { tag: image.tag} )
					.then(function(response){
						ToastService.showSimpleToast("Tag modificado con éxito");
					});
			};

			vm.disableTagChange = function(image){
				var imageTag = image.tag;
				
				if(vm.article.active > 0 && checkValidMandatories(imageTag)) return true;

				return false;
			};

			vm.deleteMultimedia = function(video, type){
				_.remove($scope[type + 's'], function(item){
					return item[type + 'ID'] === video[type + 'ID'];
				});
				$scope['delete' + _.upperFirst(type) + 's'].push(video);
			};

			$scope.addNewTag = function () {
				$scope.newTags.push({body: ''});
			};

			$scope.addNewAltTitle = function () {
				$scope.newAltTitles.push({body: ''});
			};

			$scope.addNewMultimedia = function(type) {
				$scope['new' + _.upperFirst(type) + 's'].push({type: type});
			};

			$scope.isDisabledSubmit = false;

			$scope.disableSubmit = function(){
				$scope.isDisabledSubmit = true;
			};

			$scope.getIframeSrc = function(youtubeID){
				const src = "https://www.youtube.com/embed/" + youtubeID  ;
				return $sce.trustAsResourceUrl(src);
			} 

			$scope.trustAsHtml = function(string) { 
				return $sce.trustAsHtml(string);
			};

            // TODO: refactorizar la lógica del procesado del formulario a otro controlador
			vm.ProcessForm = function(goNextAction){
				$scope.isDisabledSubmit = true;

				ArticlesService.checkLocked(vm.article.id)
					.then(function(response){
						if(response === true) {
							ToastService.showSimpleToast("Esta noticia está bloqueada por otro usuario, intente despues");
							$scope.isDisabledSubmit = true;
						} else {
							if($scope.articleForm.$valid){
								let checkForm = $scope.checkForm();
								if(checkForm.result){

									var processArticle;
									var newArticleId;

									vm.article.tags = UtilsService.arrayfyObject($scope.newTags);
									vm.article.alt_titles = UtilsService.arrayfyObject($scope.newAltTitles);

									var articleOperation = vm.edit ? vm.article.put() : Restangular.all(item_type).post(vm.article);

									vm.article.locked = 0;

									articleOperation
										.then(function(article){
											newArticleId = article.id;
											processArticle = article;

											var promises = [];

											promises.concat(_.map($scope.addCategorizations, function(categorization){
												return Restangular
													.all("itemcategorizations")
													.post({ itemId: newArticleId, categorizationId: categorization.id});
											}));

											promises.concat(_.map($scope.newImages, function(file){
												var formData = new FormData();

												formData.append('file', file.lfFile);

												var tag = file.tag || "anexos";

												return $http.post(ENV.API_URL + '/'+ item_type +'/'+ newArticleId +'/uploadImage/'+ tag, formData, {
													transformRequest: angular.identity,
													headers: {'Content-Type': undefined}
												});
											}));

											_.each(ENV.social_multimedia, function(mediaName){
												_.remove($scope['new' + _.upperFirst(mediaName) + 's'], function(item){
													return _.isEmpty(item[mediaName + 'ID']);
												});

												// var mediaNameAPI = mediaName === 'youtube' ? 'youtube-video' : mediaName;

												promises.concat(
													_.map($scope['new' + _.upperFirst(mediaName) + 's'], function(item){
														return Restangular
															.one(item_type, newArticleId)
															.customPOST(item, mediaName, {}, {});
													})
												);

												promises.concat(
													_.map($scope['delete' + _.upperFirst(mediaName) + 's'], function(item){
														return Restangular
															.one(item_type, newArticleId)
															.one(mediaName, item.id).remove();
													})
												)
											});

											return q.all(promises);
										})
										.then(function(response) {
											return q.all(_.map($scope.deleteCategorizations, function (deleteCategorization) {
												var filterObject = {
													'filter[where][itemId]': newArticleId,
													'filter[where][categorizationId]': deleteCategorization.id
												};

												return Restangular.all("itemcategorizations")
													.getList(filterObject)
													.then(function (itemcategorizations) {
														return itemcategorizations[0].id;
													})
													.catch(function (err) {
														console.log(err);
													});
											}));
										})
										.then(function(response){
											return q.all(_.map(response, function(categorizationId){
												return Restangular.one("itemcategorizations", categorizationId).remove()
											}));
										})
										.then(function(response){
											if(goNextAction === 'publish'){
												if(!processArticle.active){
													ToastService.showSimpleToast("Por favor, espere mientras publicamos su artículo");
													setTimeout(function() {
														Restangular
															.one(item_type, newArticleId)
															.customPOST({}, "activate", {}, {})
															.then( function(response){
																ToastService.showSimpleToast("Se ha publicado el artículo");
															})
															.finally(function(){
																$state.go("home.articles");																								
															})
															.catch(function(err){
																ToastService.showSimpleToast(err.data.error.message);
															});
													}, 5000);
												} else {
													$state.go("home.articles");												
												}
											} else {
												$state.go("home." + goNextAction, {}, {reload: true});
											}

										})
										.finally(function(){
											var action = vm.edit ? "editado" : "creado";
											ToastService.showSimpleToast("Artículo " + action);
										})
										.catch(function(err){
											if(err.status < 0) {
												var errMessage = "plataforma no disponible";
											}

											ToastService.showSimpleToast("Su "+ ENV.item_name +" no ha sido guardado: " + errMessage);

											console.error(err);
										});
								} else {
									$scope.isDisabledSubmit = false;
									console.log(checkForm);
									ToastService.showSimpleToast(checkForm.error);
								}
							} else {
								$scope.isDisabledSubmit = false;

								ToastService.showSimpleToast("Formulario no valido: debe completar los campos obligatorios");
							}
						}
					});
            };
		}

})();
