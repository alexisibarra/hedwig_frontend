(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:articlesCtrl
	* @description
	* # articlesCtrl
	* Controller of the app
	*/

	angular
		.module('articles')
		.controller('ArticleCtrl', Articles);

		Articles.$inject = ['$stateParams', '$state', '$scope', 'ArticlesService', '$sce'];

		function Articles($stateParams, $state, $scope, ArticlesService, $sce) {
			/*jshint validthis: true */
			var vm = this;

			$scope.$on('loadArticles', function(event, args) {
				$state.go("home.articles");
			});
			$scope.$on('loadArticle', function(event, args) {
				loadArticle();
			});

			$scope.goBack = function(){
				$state.go("home.articles");
			}

			var loadArticle = function(){
				ArticlesService
					.one($stateParams.id)
					.then(function(response){
						$scope.article = response;
					})
					.catch(function(err){
						if(err.status === 404){
							$scope.notFound = true;
						}
					});
			}

			loadArticle();

			$scope.trustAsHtml = function(string) { 
				return $sce.trustAsHtml(string);
			};
		}

})();
