(function() {
	'use strict';

	angular
		.module('articles')
		.factory('ArticlesService', articlesService);

		articlesService.$inject = ['$q', 'ENV', 'Restangular', 'AuthService'];

		function articlesService (q, ENV, Restangular, AuthService) {
			var articlesService = {};
			var item_type = ENV.item_type;

			var getFilterObject = function(limit, skip){
				return {
					'filter[include]': ['creatorUser','updaterUser', 'images'],
					'filter[limit]': limit, 
					'filter[skip]': skip,
					'filter[order]': 'creation_date DESC'
				};
			};

			articlesService.one = function(id){
				var filterObject = {'filter[include]': ['images', 'categorization','creatorUser','updaterUser']};

				_.each(ENV.social_multimedia, function(mediaName){
					filterObject['filter[include]'].push(mediaName);
				});

				return Restangular
					.one(item_type, id)
					.get(filterObject)
					.then(function(article){
						article.categorizations = {};

						_.each(_.groupBy(article.categorization, 'type'), function(value, key){
							article.categorizations[key] = value;
						});

						return article;
					});
			};

			articlesService.load = function(currentPage, skip, limit){
				var skip = currentPage > 0 ? (currentPage-1)*limit : currentPage*limit;
				var filterObject = getFilterObject(limit, skip);

				var queryPromise = Restangular.all(item_type).getList(filterObject)
					.then(function(articles){
						return articles;
					})
					.catch(function(err){
						console.log(err);
					});

				var countPromise = Restangular
					.all(item_type)
					.customGET("count")
					.then(function(response){
						return response.count;
					});

				return q.all([queryPromise, countPromise]);
			};

			articlesService.searchLoad = function(currentPage, skip, limit, searchQuery){
				var skip = currentPage > 0 ? (currentPage-1)*limit : currentPage*limit;

				var filterObject = getFilterObject(limit, skip);
				var baseArticles = Restangular.all(item_type);

				var queryPromise = baseArticles.customGET("search/" + searchQuery, filterObject)
					.then(function(articles){
						return articles;
					})
					.catch(function(err){
						console.log(err);
					});

				var countPromise = Restangular
					.all(item_type)
					.customGET("searchCount/" + searchQuery)
					.then(function(response){
						return response.count;
					});

				return q.all([queryPromise, countPromise]);
			};

			articlesService.categorizationLoad = function(currentPage, skip, limit, categoryId){
				var filterObject = getFilterObject(limit, skip);

				var baseCategory = Restangular
					.one("categorizations", categoryId );

				delete filterObject['filter[order]'];

				var queryPromise = baseCategory
					.customGET(item_type, filterObject)
					.then(function(response){
						return response;
					});

				var namePromise = baseCategory
					.get()
					.then(function(response){
						return response.body;
					});

				var countPromise = baseCategory
					.customGET(item_type)
					.then(function(response){
						return response.length;
					});


				return q.all([queryPromise, countPromise, namePromise]);
			};

			articlesService.lock = function(id){
				return Restangular
					.one(item_type, id)
					.post('lock', {userId: AuthService.getUserId()});
			};

			articlesService.unlock = function(id){
				return Restangular
					.one(item_type, id)
					.post('unlock', {userId: 0});
			};

			articlesService.remove = function(id){
				return Restangular
					.one(item_type, id)
					.remove();
			}

			articlesService.checkLocked = function(id){
				if (!id){
					return q.when(false);
				};

				return Restangular
					.one(item_type, id)
					.customGET('locked')
					.then(function(response){
						console.log(response);
						return false;
					});
			};

			articlesService.getNew = function(id){
				if(id){
					var filterObject = {'filter[include]': ['images', 'categorization']};

					_.each(ENV.social_multimedia, function(mediaName){
						filterObject['filter[include]'].push(mediaName);
					});

					return Restangular
						.one(item_type, id)
						.get(filterObject)
						.then(function(article){
							article.publish_date_start = new Date(article.publish_date_start);
							article.publish_date_end = new Date(article.publish_date_end);
							article.updaterUserId = AuthService.getUserId();

							return {
								article: article
							}
						});
				} else{
					let article = {
							article: {
								creatorUserId: AuthService.getUserId(),
								updaterUserId: AuthService.getUserId(),
								title: "",
								alt_titles: [],
								tags: [],
								body: "",
								publish_date_start: new Date(),
								publish_date_end: new Date(),
								images: [],
								categorization: []
							}
						};

					_.each(ENV.social_multimedia, function(mediaName){
						article[mediaName + "s"] = [];
					});

					return q.when(article);
				}
			};

			return articlesService;
		};
})();
