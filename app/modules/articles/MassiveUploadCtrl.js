(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name app.controller:articlesCtrl
     * @description
     * # articlesCtrl
     * Controller of the app
     */

    angular
        .module('articles')
        .controller('MassiveUploadCtrl', MassiveUpload);

    MassiveUpload.$inject = ['$stateParams', '$state', '$scope', '$http', 'ENV', 'AuthService', 'ToastService'];

    function MassiveUpload($stateParams, $state, $scope, $http, ENV, AuthService, ToastService) {
        var vm = this;

        $scope.getFileLink = function(type){
            // TODO: Refactorizar esto a un archivo de configuración
            return  "http://" + ENV.client_name.replace(" ","") + "-editor.s3-website-us-east-1.amazonaws.com/files/cargaMasiva." + type;
        };

        vm.BulkUpload = function(){
            var formData = new FormData();

            formData.append('file', $scope.files[0].lfFile);

            $http.post(ENV.API_URL + '/'+ ENV.item_type +'/massiveload/' + AuthService.getUserId(), formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(response){
                console.log(response);
                if(response.status === 200){
                    $state.go("home.articles");

                    ToastService.showSimpleToast("Carga realizada con éxito");
                } else {
                    ToastService.showSimpleToast("No se ha realizado la carga");

                }
            }).catch(function(response){
                console.log(response);
                ToastService.showSimpleToast("No se ha realizado la carga");

                vm.errors = response.data.error.details;
            });
        };
    }

})();
