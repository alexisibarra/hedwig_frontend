(function() {
    'use strict';

    angular
        .module('articles')
        .factory('UtilsService', utilsService);

    utilsService.$inject = [];

    function utilsService () {
        var utilsService = {};

        utilsService.getImageDimensions = function(imageUrl) {
            var img = new Image();

            img.onload = function(){};

            img.src = imageUrl;

            return {width: img.width, height: img.height};
        };

        utilsService.getItemIndex = function(item, list){
            return _.findIndex(list, function(listItem) { return listItem.id === item.id; });
        };

        utilsService.itemExists = function(item, list){
            if(list.length <= 0 ){
                return false
            }

            return utilsService.getItemIndex(item, list) > -1;
        };

        utilsService.arrayfyObject = function(objectIn) {
            return _(objectIn)
                .map(function(tag) { return tag.body})
                .uniqWith(_.isEqual)
                .without('')
                .value();
        };

        utilsService.objectifyArray = function(arrayIn) {
            return _.map(arrayIn, function (elem) {
                return {body: elem};
            })
        }

        return utilsService;
    }
})();
