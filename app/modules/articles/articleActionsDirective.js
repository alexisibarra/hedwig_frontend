(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:usersCtrl
	 * @description
	 * # usersCtrl
	 * Controller of the app
	 */

	angular
		.module('users')
		.directive('articleActions', Directive);

	var scripts = document.getElementsByTagName("script");
	var currentScriptPath = scripts[scripts.length-1].src;

	Directive.$inject = [];

	function Directive() {
		return {
			controller: 'ArticleActionsCtrl',
			templateUrl: currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1) + 'views/articleActions.html'
		};
	}

})();

