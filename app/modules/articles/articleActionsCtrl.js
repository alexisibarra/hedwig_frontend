(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:articlesCtrl
	 * @description
	 * # articlesCtrl
	 * Controller of the app
	 */

	angular
		.module('articles')
		.controller('ArticleActionsCtrl', ArticleActions);

	ArticleActions.$inject = ['$scope', '$state', 'Restangular', 'ENV', '$mdDialog', 'ToastService', 'AuthService', 'ArticlesService'];

	function ArticleActions($scope, $state, Restangular, ENV, $mdDialog, ToastService, AuthService, ArticlesService) {

		var item_type = ENV.item_type;

		$scope.showDestacado = ENV.destacado;

		$scope.changeStatus = function(id, status){
			var action = status ?
				{ english: "activate", spanish: "activado"} :
				{ english: "deactivate", spanish: "desactivado"};

			Restangular
				.one(item_type, id)
				.customPOST({}, action.english, {}, {})
				.then( function(response){
					if($state.current.name === 'home.articles'){
						$scope.$emit('loadArticles', {});
					} else {
						$scope.$emit('loadArticle', {});

						$scope.article.active = response.active;
					}

					ToastService.showSimpleToast("Se ha " + action.spanish + " el artículo");
				})
				.catch(function(err){
					ToastService.showSimpleToast(err.data.error.message);
				});
		};

        $scope.checkLocked = function(lockedId){
            return (lockedId !==0 && lockedId !== parseInt(AuthService.getUserId(), 10));
        };

		$scope.deleteConfirm = function(article) {
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title('¿Quiere eliminar el articulo \'' +article.title +'\'?')
				.textContent('Esta acción es irreversible')
				.ariaLabel('Lucky day')
				.ok('Si, eliminalo')
				.cancel('No, cancela');

			$mdDialog.show(confirm).then(function() {
				$scope.status = 'You decided to get rid of your debt.';

				ArticlesService
					.remove(article.id)
					.then(function(article){
						$scope.$emit('loadArticles', {});

						ToastService.showSimpleToast("Articulo eliminado");
					})
					.catch(function(err){
						console.log(err);
					});

			}, function() {
				$scope.status = 'You decided to keep your debt.';
			});
		};

		$scope.canUnlock = function(article){
			if(!article){
				 return false; 
			}

			return (AuthService.isChief() || AuthService.isAdmin() || (article.locked === parseInt(AuthService.getUserId(), 10)));
		};

		$scope.unlockConfirm = function(article) {
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title('¿Quiere desbloquar el articulo \'' +article.title +'\'?')
				.textContent('Algún otro usuario puede estar trabajando aun')
				.ariaLabel('Lucky day')
				.ok('Si, desbloquealo')
				.cancel('No, cancela');

			$mdDialog.show(confirm).then(function() {
				ArticlesService
					.unlock(article.id)
					.then(function(article){
						console.log("Articulo desbloqueado", article);

						$scope.$emit('loadArticles', {});

						ToastService.showSimpleToast("Articulo desbloqueado");
					})
					.catch(function(err){
						console.log(err);
					});

			}, function() {
				$scope.status = 'You decided to keep your debt.';
			});
		};
	}

})();
