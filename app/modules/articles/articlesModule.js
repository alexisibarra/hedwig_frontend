(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:articleModule
	 * @description
	 * # articleModule
	 * Module of the app
	 */

	angular.module('articles', ['ngMaterial', 'ngMessages', 'toast']);

})();
