(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name app.controller:ListCtrl
     * @description
     * # ListCtrl
     * Controller of the app
     */

    angular
        .module('articles')
        .controller('ListCtrl', ListArticles);

    ListArticles.$inject = ['$stateParams', '$state', '$scope', 'Restangular', 'ENV', 'ArticlesService', 'CategorizationsService'];

    function ListArticles($stateParams, $state, $scope, Restangular, ENV, ArticlesService, CategorizationsService) {
        var vm = this;
        $scope.limit = 10;
        $scope.current = {
            page: 0
        };
        $scope.item_name = ENV.item_name;
        $scope.config = {};
        $scope.config.massive_load = ENV.massive_load;

        $scope.pageChanged = function() {
            $scope.loadArticles();
        };

        $scope.$on('loadArticles', function(event, args) {
            $scope.loadArticles();
        });

        vm.loadCategorizations = CategorizationsService.load()
            .then(function(response){
                $scope.Categories = response[0];
                $scope.Zones = response[1];
            });

        $scope.loadArticles = function(){
            ArticlesService.load($scope.current.page, $scope.skip, $scope.limit)
                .then(function(response){
                    $scope.articles = response[0];
                    $scope.totalItems = response[1];
                });
        };

        if($state.current.name === 'home.articlesSearch'){
            vm.searchQuery = $stateParams.searchQuery;

            ArticlesService.searchLoad($scope.currentPage, $scope.skip, $scope.limit, vm.searchQuery)
                .then(function(response){
                    $scope.articles = response[0];
                    $scope.totalItems = response[1];

                });
        } else if($state.current.name === 'home.articlesByCategory') {
            ArticlesService.categorizationLoad($scope.currentPage, $scope.skip, $scope.limit, $stateParams.categoryId)
                .then(function(response){
                    $scope.articles = response[0];
                    $scope.totalItems = response[1]
                    vm.categorizationName = response[2];
                });
        } else {
            $scope.loadArticles();
        }

        vm.openFilterMenu = function ($mdOpenMenu, ev) {
            if(_.isEmpty($scope.Zones) || _.isEmpty($scope.Categories))
                vm.loadCategorizations();
            $mdOpenMenu(ev);
        };
    }
})();
