(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:categoriesCtrl
	* @description
	* # categoriesCtrl
	* Controller of the app
	*/

	angular
		.module('categories')
		.controller('ListCategoriesCtrl', Categories);

		Categories.$inject = ['$stateParams', '$scope', '$state', '$mdDialog', '$mdToast', 'Restangular', 'ToastService'];

		function Categories($stateParams, $scope, $state, $mdDialog, $mdToast, Restangular, ToastService) {
			/*jshint validthis: true */
			var vm = this;
			vm.edit = false;

			$scope.limit = 10;

			$scope.currentPageCensuras = 0;
			$scope.currentPageCategories = 0;
			$scope.currentPageZones = 0;

			vm.getTotalCategorizations = function(type){
				Restangular
					.all("categorizations")
					.customGET(type)
					.then(function(response){
						$scope["total" + _.upperFirst(type)] = response.length;
					});
			};

			vm.getTotalCategorizations('categories');
			vm.getTotalCategorizations('zones');
			vm.getTotalCategorizations('censuras');

			$scope.pageChanged = function(type) {
				vm.loadCategorizations(type);
			};

			vm.loadCategorizations = function(type){
				var currentPageValue = $scope["currentPage" + _.upperFirst(type)];
				var skip = currentPageValue > 0 ? (currentPageValue-1)*$scope.limit : currentPageValue*$scope.limit;
				var filterObject = {'filter[limit]': 10, 'filter[skip]': skip};

				Restangular
					.all("categorizations")
					.customGET(type, filterObject)
					.then(function(categories){
						$scope[type] = categories;
					});
			};

			vm.loadCategorizations('zones');
			vm.loadCategorizations('categories');
			vm.loadCategorizations('censuras');

			vm.deleteConfirm = function(category) {
				console.log(category);

				// Appending dialog to document.body to cover sidenav in docs app
				var confirm = $mdDialog.confirm()
					.title('¿Quiere eliminar la categoria \'' +category.body +'\'?')
					.textContent('Esta acción es irreversible')
					.ariaLabel('Lucky day')
					.ok('Si, eliminala')
					.cancel('No, cancela');

				$mdDialog.show(confirm).then(function() {
					$scope.status = 'You decided to get rid of your debt.';

					Restangular
						.one("categorizations", category.id)
						.remove()
						.then(function(category){
							console.log("Categorizacion eliminada", category);
							vm.loadCategorizations();
							ToastService.showSimpleToast("Categorizacion eliminada");
						})
						.catch(function(err){
							console.log(err);
						});

				}, function() {
					$scope.status = 'You decided to keep your debt.';
				});
			};
		}
})();
