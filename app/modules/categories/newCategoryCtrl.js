(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:categoriesCtrl
	* @description
	* # categoriesCtrl
	* Controller of the app
	*/

	angular
		.module('categories')
		.controller('NewCategoryCtrl', Categories);

		Categories.$inject = ['$stateParams', '$scope', '$state', '$mdDialog', '$mdToast', 'Restangular', 'ToastService'];

		function Categories($stateParams, $scope, $state, $mdDialog, $mdToast, Restangular, ToastService) {
			/*jshint validthis: true */
			var vm = this;
			vm.edit = false;


			if($state.current.name === 'home.newcategory'){
				vm.category = {
					body: "",
					type: ""
				};
			} else if($state.current.name === 'home.editcategory'){
				vm.edit = true;

				Restangular
					.one('categorizations', $stateParams.id)
					.get()
					.then(function(category){
						vm.category = category;
					});
			}

			vm.types = [];

			Restangular
				.all('categorizations')
				.customGET("types")
				.then(function(types){
					_.each(types, function(type){
						vm.types.push({value: type, tag: _.upperFirst(type)})
					})
				});

			vm.ProcessForm = function(){
				var categorizationOperation = vm.edit ? vm.category.put() : Restangular.all("categorizations").post(vm.category);

				categorizationOperation
					.then(function (category) {
						console.log("Categorizacion creada", category);
						$state.go("home.categories");
						ToastService.showSimpleToast("Categorizacion creada");
					})
					.catch(function(err){
						console.log(err);

						switch(err.status){
							case 422:
								_.each(err.data.error.details.messages, function(value){
									ToastService.showSimpleToast(value[0]);
								});
								break;
							default:
								console.log(err);
						}
					});
			};

			vm.deleteConfirm = function(category) {
				console.log(category);

				// Appending dialog to document.body to cover sidenav in docs app
				var confirm = $mdDialog.confirm()
					.title('¿Quiere eliminar la categoria \'' +category.body +'\'?')
					.textContent('Esta acción es irreversible')
					.ariaLabel('Lucky day')
					.ok('Si, eliminala')
					.cancel('No, cancela');

				$mdDialog.show(confirm).then(function() {
					$scope.status = 'You decided to get rid of your debt.';

					Restangular
						.one("categorizations", category.id)
						.remove()
						.then(function(category){
							console.log("Categorizacion eliminada", category);
							vm.loadCategorizations();
							ToastService.showSimpleToast("Categorizacion eliminada");
						})
						.catch(function(err){
							console.log(err);
						});

				}, function() {
					$scope.status = 'You decided to keep your debt.';
				});
			};
		}
})();
