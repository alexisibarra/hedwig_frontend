(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:menuService
	 * @description
	 * # menuService
	 * Service of the app
	 */

	angular
		.module('hedwig')
		.factory('MenuService', Menu);
		// Inject your dependencies as .$inject = ['$http', 'someSevide'];
		// function Name ($http, someSevide) {...}

		Menu.$inject = [];

		function Menu () {

			var menu = [
					{
						link: 'articles',
							name: 'Artículos'
					},

					{
						link: 'categories',
						name: 'Categorias'
					},
					{
						link: 'users',
							name: 'Usuarios'
					}

			];

			return {
				listMenu: function () {
					return menu;
				}
			};

		}

})();
