(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:HomeCtrl
	* @description
	* # HomeCtrl
	* Controller of the app
	*/

	angular
		.module('hedwig')
		.controller('HomeCtrl', Home);

	Home.$inject = ['$scope', '$state', 'homeService', 'AuthService', 'ENV'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Home($scope, $state, homeService, AuthService, ENV) {
		/*jshint validthis: true */
		var vm = this;
		vm.title = "Hedwig: editor de contenidos para " + ENV.client_name;
		vm.version = "1.0.0";
		vm.listFeatures = homeService.getFeaturesList();
		vm.client_name = ENV.client_name;

		vm.showSearch = false;

		vm.user = {
			fullname: AuthService.getFullName()
		};

		vm.ProcessSearch = function(searchInfo){
			vm.showSearch = !vm.showSearch;

			var usersMatch = $state.current.name.match(/user/g);
			var model = 'articles';

			if (usersMatch) {
				model = 'users';
			}
			
			$state.go(`home.${model}Search`, {model: model, searchQuery: searchInfo}, {reload:true});
		}
	}

})();
