(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:sessionCtrl
	* @description
	* # sessionCtrl
	* Controller of the app
	*/

	angular
		.module('session')
		.controller('SessionCtrl', Session);

		Session.$inject = ['$scope', '$state', '$rootScope', 'authEvents', 'AuthService', '$mdToast', '$http', 'ENV', '$stateParams', 'ToastService', 'Restangular'];

		function Session ($scope, $state,$rootScope, AUTH_EVENTS, AuthService, $mdToast, $http, ENV, $stateParams, ToastService, Restangular){
			/*jshint validthis: true */
			var vm = this;

			$scope.credentials = {
				username: '',
				password: ''
			};

			$scope.requestreset = function(email){
				$http.post(ENV.API_URL + '/request-password-reset', {email: email})
					.then(function(response){
						$state.go('home.login');
						ToastService.showSimpleToast(response.data.message);
					})
					.catch(function(err){
						console.log(err);

						ToastService.showSimpleToast(err.data.message);
					});
			};

			$scope.resetpassword = function(password, confirmation, access_token){
				if(access_token){
					$http.post(ENV.API_URL + '/reset-password', {password: password, confirmation: confirmation, accessToken: access_token})
						.then(function(response){
							$state.go('home.login');

							ToastService.showSimpleToast(response.data.message);
						})
						.catch(function(err){
							$state.go('home.resetpasswordrequest');

							ToastService.showSimpleToast(err.data.message);
						});
				} else {
					Restangular
						.one("editorusers", AuthService.getUserId())
						.get()
						.then(function(user){
							user.password = password;

							return user.put();
						})
						.then(function(response){
							$state.go('home.dashboard');

							ToastService.showSimpleToast("Contraseña modificada con éxito");
						})
						.catch(function(err){
							ToastService.showSimpleToast(err);
						});
				}
			};

			if($state.current.name === 'home.resetpassword'){
				$scope.access_token  = $stateParams.access_token;
			}

			$scope.login = function (credentials) {
				AuthService.login(credentials)
					.then(function (user) {
						$rootScope.$broadcast(AUTH_EVENTS.LOGIN_SUCCESS);
						$scope.setCurrentUser(user);
						$state.go('home.articles', {}, {reload: true});
						ToastService.showSimpleToast(AUTH_EVENTS.LOGIN_SUCCESS);
					})
					.catch(function () {
						ToastService.showSimpleToast(AUTH_EVENTS.LOGIN_FAILED);
						$rootScope.$broadcast(AUTH_EVENTS.LOGOUT_FAILED);
					});
			};
		}
})();
