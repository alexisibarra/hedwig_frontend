'use strict';

/**
 * @ngdoc function
 * @name app.route:sessionRoute
 * @description
 * # sessionRoute
 * Route of the app
 */

angular.module('session')
	.config(['$stateProvider', function ($stateProvider) {

		$stateProvider
			.state('home.login', {
				url:'/login',
				templateUrl: 'app/modules/session/views/login-form.html',
				controller: 'SessionCtrl',
				controllerAs: 'vm',
				data: {
					permissions: {
						except: 'LOGGEDIN',
						redirectTo: "home.dashboard"
					}
				}
			})
			.state('home.resetpasswordrequest', {
				url:'/resetpasswordrequest',
				templateUrl: 'app/modules/session/views/request-form.html',
				controller: 'SessionCtrl',
				controllerAs: 'vm',
				data: {
					permissions: {
						except: 'LOGGEDIN',
						redirectTo: "home.dashboard"
					}
				}
			})
			.state('home.changepassword', {
				url:'/changepassword',
				templateUrl: 'app/modules/session/views/password-form.html',
				controller: 'SessionCtrl',
				controllerAs: 'vm',
				data: {
					permissions: {
						only: 'LOGGEDIN',
						redirectTo: "home.login"
					}
				}
			})
			.state('home.resetpassword', {
				url:'/resetpassword?access_token',
				templateUrl: 'app/modules/session/views/password-form.html',
				controller: 'SessionCtrl',
				controllerAs: 'vm',
				data: {
					permissions: {
						except: 'LOGGEDIN',
						redirectTo: "home.dashboard"
					}
				}
			})
			.state('home.resetresponse', {
				url:'/resetresponse',
				templateUrl: 'app/modules/session/views/reset-response.html',
				controller: 'SessionCtrl',
				controllerAs: 'vm',
				data: {
					permissions: {
						except: 'LOGGEDIN',
						redirectTo: "home.dashboard"
					}
				}
			});


	}]);
