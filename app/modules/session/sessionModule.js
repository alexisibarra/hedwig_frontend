(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:sessionModule
	 * @description
	 * # sessionModule
	 * Module of the app
	 */

	angular.module('session', ['auth', 'toast']);

})();
