(function() {
	'use strict';

	angular
		.module('auth')
		.factory('AuthService', function ($http, $window, $state, ENV) {
			var authService = {};
			var tokenName = 'laiguana-token';
			var tokenDate = 'laiguana-tokendate';
			var tokenFullName = 'laiguana-fullname';
			var tokenRole = 'laiguana-role';
			var tokenUserId = 'laiguana-userId';

			var getToken = function() {
				return $window.localStorage[tokenName];
			};
			var getTokenDate = function() {
				return $window.localStorage[tokenDate];
			};
			var getRole = function() {
				return $window.localStorage[tokenRole];
			};
			var getFullName = function() {
				return $window.localStorage[tokenFullName];
			};
			var getUserId = function() {
				return $window.localStorage[tokenUserId];
			};
			var compareDates = function(date1, date2){
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24)); 
				return diffDays;
			}

			var setSession = function(token, fullname, role, userId) {
				resetToken();

				$window.localStorage[tokenUserId] = userId;
				$window.localStorage[tokenName] = token;
				$window.localStorage[tokenFullName] = fullname;
				$window.localStorage[tokenRole] = role;
				$window.localStorage[tokenDate] = new Date();
			};
			var resetToken = function() {
				$window.localStorage[tokenUserId] = '';
				$window.localStorage[tokenName] = '';
				$window.localStorage[tokenFullName] = '';
				$window.localStorage[tokenRole] = '';
			};

			authService.logout = function () {
				return resetToken();
			};
			authService.getFullName = function () {
				return getFullName();
			};
			authService.getUserId = function () {
				return getUserId();
			};

			authService.login = function (credentials) {
				return $http
					.post(ENV.API_URL + '/EditorUsers/login', credentials)
					.then(function (res) {
						setSession(res.data.id, res.data.fullname, res.data.role, res.data.userId );

						return res.data;
					})
					.catch(function(err){
						console.log(err);
						throw(err);
					});
			};
			authService.isAuthenticated = function () {
				if(compareDates(new Date(getTokenDate()), new Date()) >= 1){
					resetToken();
				}
				return !!getToken();
			};
			authService.isAdmin = function () {
				return getRole() === "Admin";
			};
			authService.isChief = function () {
				return getRole() === "Chief";
			};

			authService.isAuthorized = function (authorizedRoles) {
				if (!angular.isArray(authorizedRoles)) {
					authorizedRoles = [authorizedRoles];
				}
				return (authService.isAuthenticated() &&
				authorizedRoles.indexOf(Session.userRole) !== -1);
			};

			return authService;
		});
})();
