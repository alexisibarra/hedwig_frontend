(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:toastModule
	 * @description
	 * # toastModule
	 * Module of the app
	 */

	angular.module('toast', []);

})();
