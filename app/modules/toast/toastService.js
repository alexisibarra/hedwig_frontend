(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:toastService
	 * @description
	 * # toastService
	 * Service of the app
	 */

    angular
		.module('toast')
		.factory('ToastService', Toast);
		// Inject your dependencies as .$inject = ['$http', 'someSevide'];
		// function Name ($http, someSevide) {...}

		Toast.$inject = ['$mdToast'];

		function Toast ($mdToast) {
			var toastService = {};

			var last = {
				bottom: false,
				top: true,
				left: false,
				right: true
			};

			function getToastPosition() {
				sanitizePosition();

				return Object.keys(last)
					.filter(function(pos) { return last[pos]; })
					.join(' ');
			}

			function sanitizePosition() {
				var current = last;

				if ( current.bottom && last.top ) {
					current.top = false;
				}
				if ( current.top && last.bottom ) {
					current.bottom = false;
				}
				if ( current.right && last.left ) {
					current.left = false;
				}
				if ( current.left && last.right ) {
					current.right = false;
				}

				last = angular.extend({},current);
			}

			toastService.showSimpleToast = function(message) {
				var pinTo = getToastPosition();

				$mdToast.show(
					$mdToast.simple()
						.textContent(message)
						.position(pinTo )
						.hideDelay(3000)
				);
			};
			return toastService;
		}

})();
