(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:configModule
	 * @description
	 * # configModule
	 * Module of the app
	 */

  	angular.module('config', []);

})();
