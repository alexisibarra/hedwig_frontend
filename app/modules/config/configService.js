(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:configService
	 * @description
	 * # configService
	 * Service of the app
	 */

  	angular
		.module('config')
		.service('movie', function($http, lodash) {
			var data = null;
			var env = null;
			var API_URL;

			// var envPromise = $http.get('../../../env.json')
			// 	.then(function(response){
			//
			// 		return response.data.env;
			// 	})
			// 	.then(function(env){
			// 		console.log(env);
			//
			// 		return $http.get('../../../env.'+ env.env +'json')
			// 	})
			// 	.then(function(response){
			// 		API_URL = response.API_URL;
			// 	})
			// 	.catch(function(err){
			// 		console.log(err);
			// 	});
			// var env = $http.get('../../../env.json')
			// 	.success(function(response){
			// 		console.log(response)
			// 	})
			// 	.error(function(response){
			// 		console.log(response);
			// 	});



			var promise = $http.get('http://localhost:3000/api')
				.success(function (response) {
					console.log("> Getting config data");
					data = response;
				});



			return {
				// promise:promise,
				// setData: function (data) {
				// 	data = data;
				// },
				init: function(){
					return $http.get('http://localhost:3000/api')
						.success(function (response) {
							setTimeout(3000000, function(){
								console.log("> Getting config data");
								data = response;
							})
						});
				},
				get: function (field) {
					return data.config[field];//.getSomeData();
				}
			};
		});
})();

//
// (function() {
// 	'use strict';
//
// 	/**
// 	 * @ngdoc function
// 	 * @name app.service:configService
// 	 * @description
// 	 * # configService
// 	 * Service of the app
// 	 */
//
// 	angular
// 		.module('config')
// 		.service('movie', function($http, lodash) {
// 			var data = null;
// 			var env = null;
// 			var API_URL;
//
// 			var envPromise = $http.get('http://localhost:3000/api')
// 			// var envPromise = $http.get('../../../env.json')
// 			// 	.then(function(response){
// 			// 		return response.data.env;
// 			// 	})
// 			// 	.then(function(env){
// 			// 		return $http.get('../../../env.'+ env +'.json')
// 			// 	})
// 			// 	.then(function(response){
// 			// 		return response.data.API_URL;
// 			// 	})
// 			// 	.then(function(API_URL) {
// 			// 		console.log("> Getting config data");
// 			// 		return $http.get(API_URL)
// 			// 	})
// 				.then(function (response) {
// 					data = response;
// 				})
// 				.catch(function(err){
// 					console.log(err);
// 				});
//
// 			return {
// 				// promise:promise,
// 				// setData: function (data) {
// 				// 	data = data;
// 				// },
// 				get: function (field) {
// 					return data.config[field];//.getSomeData();
// 				}
// 			};
// 		});
// })();

