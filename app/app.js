(function() {
	'use strict';

	/**
	 * @ngdoc index
	 * @name app
	 * @description
	 * # app
	 *
	 * Main modules of the application.
	 */

	angular.module('hedwig', [
		'ngResource',
		'ngMessages',
		'ngAria',
		 'ui.bootstrap',
		 'ngMaterial',
		'lfNgMdFileInput',
		'ngMdIcons',
		'ngCookies',
		'ngAnimate',
		'ngTouch',
		'ngSanitize',
		'ui.router',
		'ngLodash',
		'restangular',
		'home',
		'session',
		'application',
		'categories',
		'configuration',
		'articles',
		'users',
		'ENV',
		'permission',
		'permission.ui',
		'config',
		'toast',
		'froala',
		'ngPatternRestrict'
	]);

})();
